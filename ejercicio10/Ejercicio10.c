#include <stdio.h>
#include <string.h>

typedef struct {
	char nombre[40];
	int dui;
	float sueldo;
}empleado;

void obtenerEmpleados();
void ordenarPorNombre();
void ordenarPorSalario();
void calcularPromedio();
void imprimirEmpleados();

int main(){
	//array de la estructua empleado de 1000
	empleado empleados[1000]; 
	//contador de empleados
	int contEmp=0;	
	int opcion=1;
	char continuar='s';
	printf("<<>BIENVENIDO>>\n");
	do{ 
		do{
			printf("--------------------------------------------------");
			printf("\n<<SELECCIONE UNA OPCION>>\n(1)-Agregar Empledo\n(2)-Ordenar por el nombre e imprimir\n(3)-Ordenar por el sueldo e imprimir\n(4)-Calcular e imprimir promedio\n(5)-Imprimir\n(6)-Salir\n\nOpcion: ");
			scanf("%i",&opcion);
			printf("--------------------------------------------------");
		}while(opcion<1 || opcion>6);
		
	
		switch(opcion){
			case 1:
				printf("\n<<AGREGAR EMPLEADO #%i>>",(contEmp+1));
				obtenerEmpleados(empleados,contEmp);
				contEmp++;
				break;
			case 2:
				ordenarPorNombre(empleados,contEmp);
				imprimirEmpleados(empleados,contEmp);
				break;
			case 3:
				ordenarPorSalario(empleados,contEmp);
				imprimirEmpleados(empleados,contEmp);
				break;
			case 4:
				calcularPromedio(empleados,contEmp);
				break;
			case 5:
				imprimirEmpleados(empleados,contEmp);
				break;
			case 6:
				printf("\n<<ADIOS>>");
				continuar='n';
				break;
		}
				
		
	}while(continuar=='s');
	
	
	
	return 0;
} 

void obtenerEmpleados(empleado *empleados, int contEmp){
	printf("\nIngese el nombre: ");
	fgets(empleados[contEmp].nombre,40,stdin);
	fgets(empleados[contEmp].nombre,40,stdin);
	fflush(stdin);
		
	printf("Ingrese el dui: ");
	scanf("%i",&empleados[contEmp].dui);
		
	printf("Ingrese el sueldo: ");
	scanf("%f",&empleados[contEmp].sueldo);
}

void ordenarPorNombre(empleado *empleados,int contEmp){
	int comparacion;
	empleado temporal;
	
	
	//ordenar de menor a mayor
	for(int i = 0; i < contEmp; i++){
		for(int j = 0; j < contEmp; j++){
			comparacion=strcmp((empleados[i].nombre),(empleados[j].nombre));
			if(comparacion<0){
				//Asignamos un valor temporal
				strcpy(temporal.nombre,empleados[j].nombre);
				temporal.dui=empleados[j].dui;
				temporal.sueldo=empleados[j].sueldo;
				
				//cambiamos el valor en palabras[j].texto por palabras[i].texto
				strcpy(empleados[j].nombre,empleados[i].nombre);
				empleados[j].dui=empleados[i].dui;
				empleados[j].sueldo=empleados[i].sueldo;
				
				//Cambiamos el valor en palabras[i].texto por el temporal
				strcpy(empleados[i].nombre,temporal.nombre);
				empleados[i].dui=temporal.dui;
				empleados[i].sueldo=temporal.sueldo;
			}
		}	
	}	
}

//Ordenar por salario
void ordenarPorSalario(empleado *empleados, int contEmp){
	empleado temporal;
//ordenar de mayor a menor
	for(int i = 0; i < contEmp; i++){
		for(int j = 0; j < contEmp; j++){
			
			if(empleados[i].sueldo>empleados[j].sueldo){
				//Asignamos un valor temporal
				strcpy(temporal.nombre,empleados[j].nombre);
				temporal.dui=empleados[j].dui;
				temporal.sueldo=empleados[j].sueldo;
				
				//cambiamos el valor en palabras[j].texto por palabras[i].texto
				strcpy(empleados[j].nombre,empleados[i].nombre);
				empleados[j].dui=empleados[i].dui;
				empleados[j].sueldo=empleados[i].sueldo;
				
				//Cambiamos el valor en palabras[i].texto por el temporal
				strcpy(empleados[i].nombre,temporal.nombre);
				empleados[i].dui=temporal.dui;
				empleados[i].sueldo=temporal.sueldo;
			}
		}	
	}	
}

void calcularPromedio(empleado *empleados,int contEmp){
	float promedio=0;
	if(contEmp>0){
		for(int i=0;i<contEmp;i++){
			promedio=promedio+empleados[i].sueldo;
		}
		promedio=promedio/contEmp;
	}else {
		promedio=0;
	}
	printf("\nEl promedio de todos los salarios es: $%.2f\n",promedio);
	
}

//imprime la lista de empleados
void imprimirEmpleados(empleado *empleados, int contEmp){
	printf("\n<<EMPLEADOS>>\n");
	if(contEmp>0){
		for(int i=0;i<contEmp;i++){
			printf("\nEMPLEADO #%i\n",(i+1));
			printf("Nombre: %s",empleados[i].nombre);
			printf("DUI: %i\n",empleados[i].dui);
			printf("Sueldo: $%.2f\n",empleados[i].sueldo);
		}
	}else{
		printf("\n¡No hay empleados agregados!\n");
	}
	//printf("--------------------------------------------------\n");
}
