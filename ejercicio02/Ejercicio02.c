#include <stdio.h>

//la funcion tiene como parametro un puntero
//recibe como argumento una direccion de memoria
void imprimir(int *a){
	//suma +1 al valor hacia donde apunta la direccion de memoria
	*a = *a + 1;
	//imprime el valor hacia donde apunta la direccion de memoria
	printf("%d\n", *a);
	return;
}

int main(){
	int i , a = 0;
	
	for(i = 0; i < 5; i++){
		//Enviamos como argumento la direccion de memoria de la variable a
		imprimir(&a);
	}
	
	return 0;
}

//Imprime 1 2 3 4 5
