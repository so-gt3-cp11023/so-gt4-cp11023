#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void llenadoAleatorio();
void ordenarLista();
void imprimirLista();

int main(){
	//indica el tamaño del vector
	int n=24;
	
	int numeros[n];
	
	//llenado aleatorio
	llenadoAleatorio(numeros,n);
	
	
	printf("Lista sin ordenar:\n");
	imprimirLista(numeros,n);
	
	printf("\n\nLista ordenada:\n");
	
	ordenarLista(numeros,n);
	imprimirLista(numeros,n);
	
	

	return 0;
}

//funcion llenado automatico de numeros aleatorios
void llenadoAleatorio(int *numeros, int n){
	srand (time(NULL));
	for(int i = 0; i < n; i++){
		numeros[i] = rand() % 101;
	}
}

//funcion para ordenar
void ordenarLista(int *numeros, int n){	
	int temporal=0;
	
	//ordenar de mayor a menor con punteros
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if(numeros[i]>numeros[j]){
				temporal = *(numeros+j);
				*(numeros+j) = *(numeros+i);
				*(numeros+i) = temporal;
			}
		}	
	}	
	
}

//funcion para imprimir
void imprimirLista(int *numeros,int n){
	for(int i=0;i<n;i++){
		printf("%i,",numeros[i]);
	}
}
