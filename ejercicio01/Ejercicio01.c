#include <stdio.h>


int main(){
	//guardan valores
	float n1;
	float n2;
	
	//punteros guardan direcciones de memoria
	float *p1;
	float *p2;
	
	n1=4.0;
	//en p1 se guarda la direccion de memoria de n1
	p1=&n1;
	//en p2 se almacena la direccion de memoria de n1 guardada en p1
	p2=p1;
	//en n2 se guarda el valor que apunta la direccion de memoria en p2---->4.0
	//n2=4.0
	n2=*p2;
	//en se suma los valores hacia donde apuntan las direcciones de memoria p1 y p2
	//*p1=4.0, *p2=4.0 
	//n1=8.0
	n1=*p1+*p2;
	
	
	printf("n1: %f",n1);
	printf("\n");
	printf("n2: %f",n2);
	
return 0;
}

//vale n1=8 y n2=4.0
