#include <stdio.h>
#include <math.h>

void obtenerCoeficientesPolinomio();
double evaluarPolinomio();
double potencia();

int main(){
	
	//n es el grado
	int n;
	double x;
	printf("Ingrese el grado del polinomio que desea: ");
	scanf("%i",&n);
	n=n+1;
	

	double coeficientes[n];	
	obtenerCoeficientesPolinomio(coeficientes,n);
	
	//ingreso de lo que desea evaluar
	printf("\nIngrese el valor que quiere evaluar en 'X': ");
	scanf("%lf",&x);
	
	//imprimir polinomio
	printf("\nP(x)=");
	for(int i=0;i<n;i++){
		if(i>1){
			if(coeficientes[i]>=0){
				printf("+%.2lf X^%i",coeficientes[i],i);
			}else{
				printf("%.2lf X^%i",coeficientes[i],i);
			}
			
		}else if(i==1){
			if(coeficientes[i]>=0){
				printf("+%.2lfX",coeficientes[i]);
			}else{
				printf("%.2lfX",coeficientes[i]);
			}
			
		}else{
			if(coeficientes[i]>=0){
				printf("%.2lf",coeficientes[i]);
			}else{
				printf("%.2lf",coeficientes[i]);
			}
			
		}
	}
	
	
	//evaluar polinomio
	double resultado;
	resultado=evaluarPolinomio(coeficientes,n,x);
	printf("\n\nP(%.2lf)=%.2lf",x,resultado);
	
	return 0;
}





void obtenerCoeficientesPolinomio(double *coeficientes, int n){
	for(int i=0;i<n;i++){
		if(i>1){
			printf("\nIngrese el coeficiente de aX^%i :",i);
			scanf("%lf",coeficientes+i);
		}else if(i==1){
			printf("\nIngrese el coeficiente de aX:");
			scanf("%lf",coeficientes+i);
		}else{
			printf("\nIngrese el coeficiente de a:");
			scanf("%lf",coeficientes+i);
		}
	}	
} 

double evaluarPolinomio(double *coeficientes,int n,double x){
	double resultado;
	for(int i=0;i<n;i++){
		resultado=resultado+*(coeficientes+i)*potencia(x,i);
	}
	return resultado;
}

double potencia(double exponente, int potencial){
	double resultado=1;
	for(int i=0;i<potencial;i++){
		resultado=resultado*exponente;
	}
	return resultado;
}
