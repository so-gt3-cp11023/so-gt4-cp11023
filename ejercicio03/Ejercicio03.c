#include <stdio.h>

//funcion recibe como parametro una variable llamada 'a'
void imprimir(int a){
	a = a + 1;
	printf("%d\n", a);
}

int main(){
	
	int i, a = 0;
	for(i = 0; i<7; i++){
		//esta variable 'a' es el argumento que enviamos
		imprimir(a);
	}
	
	return 0;
}

//imprime 1 siete veces, porque la variable 'a' en el main siempre es 0
