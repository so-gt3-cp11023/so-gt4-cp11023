#include <stdio.h>
#include <string.h>


char *pedirTexto();
void contarVocales(char *,int[]);
void imprimir(int []); 

int main(){
	char *texto;
	int num[5];
	texto=pedirTexto();
	contarVocales(texto,num);
	imprimir(num);
	
	return 0;
}

//--------------------------------------------------------------------

char *pedirTexto(){
	char texto[100];
	char *ptr_texto;
	
	printf("Introduzca un texto: ");
	fgets(texto,100,stdin);//stdin estandar input, entrada teclado, ademas parametros string,tamaño,stdin	
	fflush(stdin);//si volvemos a pedir algo en el teclado, esto limpia
	
	//printf("\n%s",texto);
	ptr_texto=texto;
	
	return ptr_texto;
}


void contarVocales(char *ptr_texto,int num[]){
	num[0]=num[1]=num[2]=num[3]=num[4]=0;
	
	//calcular longitud de la cadena
	long tamanio = 0;
    while (ptr_texto[tamanio] != '\0') {
        tamanio++;
    }
    
	//printf("%li\n",strlen(ptr_texto)) me fallaba esta funcion en unas ejecuciones devolvía cero en otras no
	 
	for(int i=0;i<tamanio;i++){
		//printf("%c\n",ptr_texto[i]);
		if(ptr_texto[i]=='a' || ptr_texto[i]=='A'){
			num[0]+=1;
		}
		if(ptr_texto[i]=='e' || ptr_texto[i]=='E'){
			num[1]+=1;
		}
		if(ptr_texto[i]=='i' || ptr_texto[i]=='I'){
			num[2]+=1;
		}
		if(ptr_texto[i]=='o' || ptr_texto[i]=='O'){
			num[3]+=1;
		}
		if(ptr_texto[i]=='u' || ptr_texto[i]=='U'){
			num[4]+=1;
		}
	}
	
	
}

void imprimir(int num[]){
	printf("\na= %i\n",num[0]);
	printf("\ne= %i\n",num[1]);
	printf("\ni= %i\n",num[2]);
	printf("\no= %i\n",num[3]);
	printf("\nu= %i\n",num[4]);
}

