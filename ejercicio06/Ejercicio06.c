#include <stdio.h>

void obtenerDatos();
float maximo();
float minimo();
float mediaAritmetica();


int main(){
	int num_datos;
	printf("<<CALCULAR MAXIMO,MINIMO Y MEDIA ARITMETICA>>\n");
	printf("Introduzca el número de datos que desea ingresar: ");
	scanf("%i",&num_datos);
	
	float datos[num_datos];
	
	//obteniendo los datos
	obtenerDatos(datos,num_datos);
		
	printf("\nEl maximo es:  %.2f .",maximo(datos,num_datos));
	printf("\nEl minimo es:  %.2f .",minimo(datos,num_datos));
	printf("\nLa media aritmetica es: %.2f",mediaAritmetica(datos,num_datos));
	return 0;
}

//obtener datos
void obtenerDatos(float *datos, int num_datos){
	for(int i=0;i<num_datos;i++){
		printf("Introduzca un dato: ");
		scanf("%f",(datos+i));
	}
}

//obtenenmos el mayor
float maximo(float *ptr_datos, int num_datos){
	float max=0;
	if(num_datos>1){
		for(int i=0;i<(num_datos-1);i++){
				if(ptr_datos[i]>ptr_datos[i+1] && max<ptr_datos[i]){
					max=ptr_datos[i];
				}else if(ptr_datos[i]<ptr_datos[i+1]&& max<ptr_datos[i+1]){
					max=ptr_datos[i+1];
				}
		}
		
	}else if(num_datos==1){
		return ptr_datos[0];
	}
	return max;
}

//obtenemos el menor
float minimo(float *ptr_datos, int num_datos){
	float min=0;
	
	if(num_datos>1){
		for(int i=0;i<(num_datos-1);i++){
			if(i==0){
				if(ptr_datos[i]<ptr_datos[i+1]){
					min=ptr_datos[i];
				}else if(ptr_datos[i]>ptr_datos[i+1]){
					min=ptr_datos[i+1];
				}
			}else{	
				if(ptr_datos[i]<ptr_datos[i+1] && min>ptr_datos[i]){
					min=ptr_datos[i];
				}else if(ptr_datos[i]>ptr_datos[i+1] && min>ptr_datos[i+1]){
					min=ptr_datos[i+1];
				}	
			}
				
		}
		
	}else if(num_datos==1){
		return ptr_datos[0];
	}
	return min;
}

//obtenemos el promedio o media arimetica
float mediaAritmetica(float *ptr_datos, int num_datos){
	float promedio=0;
	
	for(int i=0; i<num_datos;i++){
			promedio=promedio+ptr_datos[i];
	}
	
	return promedio=promedio/num_datos;
}

