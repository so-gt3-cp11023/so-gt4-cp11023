#include <stdio.h>
#include <string.h>

//estructura
typedef struct{
	char texto[50];
}String;

void getListString();
void printListString();
void ordenarListaDescendentemente();

int main(){
	//Tamaño del vector o numero de palabras
	int n=7;
	//Es una lista de la estructura String 
	String palabras[n];
	
	//obtenemos las palabras en la lista de estructuras
	for(int i=0;i<n;i++){
		printf("Ingrese la palabra #%i: ",(i+1));
		getListString(palabras,i);
	}
	
	//imprimimos palabras 'la lista de estructura'
	printf("\nIMPRIMIENDO PALABRAS COMO FUERON INGRESADAS:\n\n");
	for(int i=0;i<n;i++){
		printListString(palabras,i);
	}

	//Ordenar lista
	ordenarListaDescendentemente(palabras,n);	
	
	//imprimimos palabras 'la lista de estructura'
	printf("\nIMPRIMIENDO LISTA ORDENADA DE FORMA DESCENDENTE:\n\n");
	for(int i=0;i<n;i++){
		printListString(palabras,i);
	}

	
	
	
	return 0;
}

//funciones

void getListString(String palabras[],int indice){
	//obtenemos el texto
	fgets(palabras[indice].texto,50,stdin);
	//limpiamos 
	fflush(stdin);
}

void printListString(String palabras[],int indice){
	printf("Palabra #%i : %s",indice+1,palabras[indice].texto);
}

void ordenarListaDescendentemente(String *palabras, int n){
	int comparacion;
	String temporal;
	
	
	//ordenar de mayor a menor la palabra
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			comparacion=strcmp((palabras[i].texto),(palabras[j].texto));
			if(comparacion>0){
				//Asignamos un valor temporal
				strcpy(temporal.texto,palabras[j].texto);
				//cambiamos el valor en palabras[j].texto por palabras[i].texto
				strcpy(palabras[j].texto,palabras[i].texto);
				//Cambiamos el valor en palabras[i].texto por el temporal
				strcpy(palabras[i].texto,temporal.texto);
			}
		}	
	}	
}

